package CGBVP.emergency.adapters.web.modules.transcriber;

import CGBVP.emergency.commons.hexagonal.WebAdapter;
import CGBVP.emergency.intranet.transcriber.application.port.in.TranscribeMessageUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@WebAdapter
@RestController
@RequiredArgsConstructor
public class TranscribeMessageController {
    private final TranscribeMessageUseCase transcribeMessageUseCase;
    @CrossOrigin( origins = "http://localhost:3000")
    @ResponseStatus(HttpStatus.OK)
    @PostMapping(value = "/messages/transcribe", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public String registerNewMessage(@RequestPart("file") MultipartFile inputAudio) throws IOException, InterruptedException {
        return transcribeMessageUseCase.execute(inputAudio);
    }
}
