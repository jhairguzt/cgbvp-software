package CGBVP.emergency.adapters.persistence.modules.session_manager;

import CGBVP.emergency.commons.hexagonal.PersistenceAdapter;
import CGBVP.emergency.intranet.session_manager.application.port.in.UserToRegister;
import CGBVP.emergency.intranet.session_manager.application.port.out.GetUserPort;
import CGBVP.emergency.intranet.session_manager.application.port.out.GetUsersListPort;
import CGBVP.emergency.intranet.session_manager.application.port.out.RegisterUserPort;
import CGBVP.emergency.intranet.session_manager.application.port.out.UpdateUserPort;
import CGBVP.emergency.intranet.session_manager.domain.User;
import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.NotFound;

import java.util.List;
import java.util.Optional;

@PersistenceAdapter
@RequiredArgsConstructor
public class UserPersistenceAdapter implements RegisterUserPort, UpdateUserPort, GetUsersListPort, GetUserPort {
    private final UserModelRepository repository;
    private final UserMapper mapper;

    @Override
    public User register(UserToRegister userToRegister) {
        var row = mapper.toModel(userToRegister);
        return mapper.toUser(repository.saveAndFlush(row));
    }

    @Override
    public User update(UserToRegister userToRegister, Long userId) {
        var row = mapper.toModel(userToRegister);
        row.setId(userId);
        return mapper.toUser(repository.save(row));
    }

    @Override
    public List<User> getList(String name) {
        return mapper.toUserList(repository.findAllActiveUsersOrdered(name));
    }

    @Override
    public List<User> getList() {
        return mapper.toUserList(repository.findAllActiveUsersOrdered(""));
    }

    @Override
    public User getUser(Long userId) {
        var row = repository.findById(userId).orElseThrow(() -> new RuntimeException("Not Found"));
        return mapper.toUser(row);
    }
}
