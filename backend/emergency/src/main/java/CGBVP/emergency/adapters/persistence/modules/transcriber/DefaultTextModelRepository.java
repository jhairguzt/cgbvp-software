package CGBVP.emergency.adapters.persistence.modules.transcriber;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface DefaultTextModelRepository extends JpaRepository<DefaultTextModel, Long> {
    @Query(value= "SELECT * FROM texto_predeterminado where texto ilike(concat('%',?1,'%'))  order by texto desc", nativeQuery = true)
    List<DefaultTextModel> findAllDefaultTextByText(String text);
}