package CGBVP.emergency.adapters.persistence.modules.logistic;

import CGBVP.emergency.commons.hexagonal.PersistenceAdapter;
import CGBVP.emergency.intranet.logistic.application.port.in.UnitToRegister;
import CGBVP.emergency.intranet.logistic.application.port.out.RegisterUnitPort;
import CGBVP.emergency.intranet.logistic.domain.Unit;
import CGBVP.emergency.intranet.logistic.domain.UnitState;
import lombok.RequiredArgsConstructor;

@PersistenceAdapter
@RequiredArgsConstructor
public class UnitPersistenceAdapter implements RegisterUnitPort {
    private final UnitModelRepository repository;
    private final UnitMapper mapper;
    @Override
    public Unit register(UnitToRegister unitToRegister) {
        var row = mapper.toModel(unitToRegister);
        row.setState(UnitState.DISPONIBLE.toString());
        return mapper.toUser(repository.saveAndFlush(row));
    }
}
