package CGBVP.emergency.adapters.persistence.modules.transcriber;

import CGBVP.emergency.commons.hexagonal.PersistenceAdapter;
import CGBVP.emergency.intranet.transcriber.application.port.in.NewMessageToRegister;
import CGBVP.emergency.intranet.transcriber.application.port.out.GetMessagesListPort;
import CGBVP.emergency.intranet.transcriber.application.port.out.RegisterNewMessagePort;
import CGBVP.emergency.intranet.transcriber.domain.Message;
import lombok.RequiredArgsConstructor;

import java.util.List;

@PersistenceAdapter
@RequiredArgsConstructor
public class MessagePersistenceAdapter implements RegisterNewMessagePort, GetMessagesListPort {
    private final MessageModelRepository repository;
    private final MessageMapper mapper;
    @Override
    public Message register(NewMessageToRegister toRegister, Long contentId) {
        var row = mapper.toModel(toRegister);
        row.setContentId(contentId);
        return mapper.toMessage(repository.saveAndFlush(row));
    }

    @Override
    public List<Message> getList(Long emergencyId) {
        return mapper.toMessageList(repository.findAllActivesMessagesOrdered(emergencyId));
    }
}
