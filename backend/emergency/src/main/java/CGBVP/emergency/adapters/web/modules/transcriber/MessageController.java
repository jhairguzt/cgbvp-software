package CGBVP.emergency.adapters.web.modules.transcriber;

import CGBVP.emergency.commons.hexagonal.WebAdapter;
import CGBVP.emergency.intranet.transcriber.application.port.in.GetDefaultTextListUseCase;
import CGBVP.emergency.intranet.transcriber.application.port.in.GetMessagesListUseCase;
import CGBVP.emergency.intranet.transcriber.application.port.in.NewMessageToRegister;
import CGBVP.emergency.intranet.transcriber.application.port.in.RegisterNewMessageUseCase;
import CGBVP.emergency.intranet.transcriber.domain.DefaultText;
import CGBVP.emergency.intranet.transcriber.domain.Message;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;


@WebAdapter
@RestController
@RequiredArgsConstructor
@CrossOrigin(value = "*")
public class MessageController {
    private final RegisterNewMessageUseCase registerNewMessageUseCase;
    private final GetDefaultTextListUseCase getDefaultTextListUseCase;
    private final GetMessagesListUseCase getMessagesListUseCase;

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(value = "/messages/new")
    public Message registerNewMessage(@RequestBody NewMessageToRegister newMessageToRegister) {
        return registerNewMessageUseCase.execute(newMessageToRegister);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "messages/defaultTextList")
    public List<DefaultText> getDefaultTextList(){
        return getDefaultTextListUseCase.execute();
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "messages/defaultTextList/{text}")
    public List<DefaultText> getDefaultTextListByText(@PathVariable String text){
        return getDefaultTextListUseCase.execute(text);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "messages/list/{emergencyId}")
    public List<Message> getMessagesList(@PathVariable Long emergencyId) {return getMessagesListUseCase.execute(emergencyId);}

}

