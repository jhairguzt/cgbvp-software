package CGBVP.emergency.adapters.persistence.modules.emergency;

import CGBVP.emergency.intranet.emergency.domain.EmergencyState;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@RequiredArgsConstructor
@Entity @Table(name = "EMERGENCIA")
public class EmergencyModel {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "emergencia_id")
    private Long id;

    @Column(name = "fecha", nullable = false)
    private Date date;

    @Column(name = "distrito", nullable = false)
    private String district;

    @Column(name = "direccion", nullable = false)
    private String address;

    @Column(name = "tipo", nullable = false)
    private String type;

    @Column(name = "sub_tipo", nullable = false)
    private String subType;

    @Column(name = "descripcion", nullable = false)
    private String description;

    @Column(name = "state",nullable = false)
    private String emergencyState;

    @CreatedDate
    @CreationTimestamp
    @ColumnDefault("CURRENT_TIMESTAMP")
    @Column(name = "fecha_creacion")
    private Timestamp createdAt;

    @LastModifiedDate
    @UpdateTimestamp
    @ColumnDefault("CURRENT_TIMESTAMP")
    @Column(name = "fecha_modificacion")
    private Timestamp updatedAt;

    @Column(name = "eliminado")
    private boolean deleted;

    @Column(name = "compania_bomberos_id")
    private Long companyId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "compania_bomberos_id",insertable = false, updatable = false)
    private FireCompanyModel fireCompany;

    @OneToMany(mappedBy = "emergency")
    private Set<UserXEmergencyModel> userXEmergencyList = new HashSet<>();

    @OneToMany(mappedBy = "emergency")
    private Set<UnitXEmergencyModel> unitXEmergencyList = new HashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        EmergencyModel that = (EmergencyModel) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
