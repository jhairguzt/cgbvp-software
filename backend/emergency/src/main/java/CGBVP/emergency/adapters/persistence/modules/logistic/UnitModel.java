package CGBVP.emergency.adapters.persistence.modules.logistic;

import CGBVP.emergency.adapters.persistence.modules.emergency.FireCompanyModel;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Getter
@Setter
@RequiredArgsConstructor
@Entity
@Table(name = "UNIDAD")
public class UnitModel {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "unidad_id")
    private Long id;

    @Column(name = "placa",nullable = false)
    private String plate;

    @Column(name = "codigo",nullable = false)
    private String code;

    @Column(name = "tipo", nullable = false)
    private String type;

    @Column(name = "descripcion",nullable = false)
    private String description;

    @Column(name = "estado", nullable = false)
    private String state;

    @Column(name = "imagen", nullable = false)
    private String imagePath;

    @Column(name = "compania_bomberos_id", nullable = false)
    private Long fireCompanyId;

    @CreatedDate
    @CreationTimestamp
    @ColumnDefault("CURRENT_TIMESTAMP")
    @Column(name = "fecha_creacion")
    private Timestamp createdAt;

    @LastModifiedDate
    @UpdateTimestamp
    @ColumnDefault("CURRENT_TIMESTAMP")
    @Column(name = "fecha_modificacion")
    private Timestamp updatedAt;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "compania_bomberos_id", insertable = false, updatable = false)
    private FireCompanyModel fireCompany;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        UnitModel unitModel = (UnitModel) o;
        return id != null && Objects.equals(id, unitModel.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
