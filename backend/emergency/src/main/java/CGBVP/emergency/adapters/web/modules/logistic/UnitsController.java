package CGBVP.emergency.adapters.web.modules.logistic;

import CGBVP.emergency.commons.hexagonal.WebAdapter;
import CGBVP.emergency.intranet.logistic.application.port.in.RegisterUnitUseCase;
import CGBVP.emergency.intranet.logistic.application.port.in.UnitToRegister;
import CGBVP.emergency.intranet.logistic.domain.Unit;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@RestController
@RequiredArgsConstructor
@CrossOrigin(value = "*")
public class UnitsController {
    private final RegisterUnitUseCase registerUnitUseCase;

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(value = "/units/create")
    public Unit createUnit(@RequestBody UnitToRegister unitToRegister) {
        return registerUnitUseCase.execute(unitToRegister);
    }
}
