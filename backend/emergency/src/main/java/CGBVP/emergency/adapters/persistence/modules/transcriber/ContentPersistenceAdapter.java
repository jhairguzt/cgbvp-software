package CGBVP.emergency.adapters.persistence.modules.transcriber;

import CGBVP.emergency.commons.hexagonal.PersistenceAdapter;
import CGBVP.emergency.intranet.transcriber.application.port.in.ContentToRegister;
import CGBVP.emergency.intranet.transcriber.application.port.out.RegisterContentPort;
import CGBVP.emergency.intranet.transcriber.domain.Content;
import lombok.RequiredArgsConstructor;

@PersistenceAdapter
@RequiredArgsConstructor
public class ContentPersistenceAdapter implements RegisterContentPort {
    private final ContentModelRepository repository;
    private final ContentMapper mapper;
    @Override
    public Content register(ContentToRegister contentToRegister) {
        var row = mapper.toModel(contentToRegister);
        return mapper.toContent(repository.saveAndFlush(row));
    }
}
