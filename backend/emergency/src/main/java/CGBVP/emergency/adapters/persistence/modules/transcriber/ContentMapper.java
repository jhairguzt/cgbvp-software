package CGBVP.emergency.adapters.persistence.modules.transcriber;

import CGBVP.emergency.intranet.transcriber.application.port.in.ContentToRegister;
import CGBVP.emergency.intranet.transcriber.domain.Content;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {DefaultTextMapper.class})
public interface ContentMapper {
    @Mapping(target = "id", source = "id")
    @Mapping(target = "defaultText", source = "defaultText")
    @Mapping(target = "intranetText", source = "intranetText")
    @Mapping(target = "extranetText", source = "extranetText")
    @Mapping(target = "mediaPath", source = "mediaPath")
    Content toContent(ContentModel model);

    @Mapping(target = "defaultTextId", source = "defaultTextId")
    @Mapping(target = "extranetText", source = "extranetText")
    @Mapping(target = "intranetText", source = "intranetText")
    @Mapping(target = "mediaPath", source = "mediaPath")
    ContentModel toModel(ContentToRegister toRegister);
}
