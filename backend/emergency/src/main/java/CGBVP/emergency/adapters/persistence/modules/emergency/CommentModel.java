package CGBVP.emergency.adapters.persistence.modules.emergency;

import CGBVP.emergency.adapters.persistence.modules.session_manager.UserModel;
import CGBVP.emergency.adapters.persistence.modules.transcriber.MessageModel;
import lombok.*;
import org.hibernate.Hibernate;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Getter
@Setter
@RequiredArgsConstructor
@Entity
@Table(name = "COMENTARIO")
public class CommentModel {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "comentario_id")
    private Long id;

    @Column(name = "texto",nullable = false)
    private String text;

    @CreatedDate
    @Column(name = "fecha_creacion",nullable = false)
    private Timestamp createdAt;

    @LastModifiedDate
    @Column(name = "fecha_actualizacion", nullable = false)
    private Timestamp updatedAt;

    @Column(name = "mensaje_id")
    private Long messageId;

    @Column(name = "usuario_id")
    private Long userId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "mensaje_id",insertable = false, updatable = false)
    private MessageModel message;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "usuario_id",insertable = false, updatable = false)
    private UserModel user;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        CommentModel that = (CommentModel) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
