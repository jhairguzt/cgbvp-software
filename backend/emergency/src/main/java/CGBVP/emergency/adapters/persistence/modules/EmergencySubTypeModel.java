package CGBVP.emergency.adapters.persistence.modules;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;

@Getter
@Setter
@RequiredArgsConstructor
@Entity
@Table(name = "SUB_TIPO_EMERGENCIA")
public class EmergencySubTypeModel {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "sub_tipo_emergencia_id")
    private Long id;

    @Column(name = "nombre",nullable = false)
    private String name;

    @Column(name = "codigo",nullable = false)
    private String code;

    @Column(name = "tipo_emergencia_id")
    private Long emergencyTypeId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "tipo_emergencia_id", insertable = false, updatable = false)
    private EmergencyTypeModel emergencyType;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        EmergencySubTypeModel that = (EmergencySubTypeModel) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
