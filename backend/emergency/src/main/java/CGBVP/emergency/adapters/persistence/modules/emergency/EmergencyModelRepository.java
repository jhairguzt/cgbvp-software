package CGBVP.emergency.adapters.persistence.modules.emergency;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EmergencyModelRepository extends JpaRepository<EmergencyModel, Long> {
    @Query(value= "SELECT * FROM emergencia where eliminado is false and state  = ?1 order by fecha desc", nativeQuery = true)
    List<EmergencyModel> findAllEmergenciesByStatusOrdered(String state);
}