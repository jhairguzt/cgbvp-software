package CGBVP.emergency.adapters.persistence.modules.logistic;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UnitModelRepository extends JpaRepository<UnitModel, Long> {
}