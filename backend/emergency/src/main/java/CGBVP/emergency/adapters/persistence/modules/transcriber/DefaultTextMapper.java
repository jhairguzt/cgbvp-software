package CGBVP.emergency.adapters.persistence.modules.transcriber;

import CGBVP.emergency.intranet.transcriber.domain.DefaultText;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DefaultTextMapper {
    @Mapping(target = "id", source ="id")
    @Mapping(target = "text", source ="text")
    @Mapping(target = "code", source ="code")
    DefaultText toDefaultText(DefaultTextModel model);
    List<DefaultText> toDefaulTextList(List<DefaultTextModel> defaultTextModels);
}
