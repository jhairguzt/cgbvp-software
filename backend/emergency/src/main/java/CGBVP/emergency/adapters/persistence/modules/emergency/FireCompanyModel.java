package CGBVP.emergency.adapters.persistence.modules.emergency;

import CGBVP.emergency.adapters.persistence.modules.logistic.UnitModel;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@RequiredArgsConstructor
@Entity
@Table(name = "COMPANIA_DE_BOMBEROS")
public class FireCompanyModel {
    @Id @GeneratedValue( strategy = GenerationType.IDENTITY)
    @Column(name = "compania_bomberos_id")
    private Long id;

    @Column(name = "encargado",nullable = false)
    private String manager;

    @Column(name = "nombre",nullable = false)
    private String name;

    @Column(name = "distrito",nullable = false)
    private String district;

    @Column(name = "direccion",nullable = false)
    private String address;

    @Column(name = "estado",nullable = false)
    private boolean state;

    @Column(name = "eliminado", nullable = false)
    private boolean deleted;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        FireCompanyModel that = (FireCompanyModel) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}