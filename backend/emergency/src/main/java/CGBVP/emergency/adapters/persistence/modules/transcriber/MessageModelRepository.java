package CGBVP.emergency.adapters.persistence.modules.transcriber;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MessageModelRepository extends JpaRepository<MessageModel, Long> {
    @Query(value= "SELECT * FROM mensaje where eliminado is false and emergencia_id  = ?1 order by fecha_creacion desc", nativeQuery = true)
    List<MessageModel> findAllActivesMessagesOrdered (Long emergencyId);
}