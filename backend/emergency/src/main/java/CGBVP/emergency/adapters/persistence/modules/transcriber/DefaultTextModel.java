package CGBVP.emergency.adapters.persistence.modules.transcriber;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(name = "TEXTO_PREDETERMINADO")
public class DefaultTextModel {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "texto_predeterminado_id")
    private Long id;

    @Column(name = "texto",nullable = false)
    private String text;

    @Column(name = "codigo",nullable = false)
    private String code;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        DefaultTextModel that = (DefaultTextModel) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
