package CGBVP.emergency.adapters.web.modules.emergency;

import CGBVP.emergency.commons.hexagonal.WebAdapter;
import CGBVP.emergency.intranet.emergency.application.port.in.RegisterEmergencyUseCase;
import CGBVP.emergency.intranet.emergency.application.port.in.EmergencyToRegister;
import CGBVP.emergency.intranet.emergency.application.port.in.GetEmergencyListUseCase;
import CGBVP.emergency.intranet.emergency.domain.Emergency;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@WebAdapter
@RestController
@RequiredArgsConstructor
@CrossOrigin(value = "*")
public class EmergencyController {
    private final GetEmergencyListUseCase getEmergencyListUseCase;
    private final RegisterEmergencyUseCase createEmergencyUseCase;
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/emergency/list/{state}")
    public List<Emergency> getEmergencyList(@PathVariable String state) {
        return getEmergencyListUseCase.execute(state);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(value = "/emergency/create")
    public Emergency createEmergency(@RequestBody EmergencyToRegister emergencyToRegister) {
        return createEmergencyUseCase.execute(emergencyToRegister);
    }
}
