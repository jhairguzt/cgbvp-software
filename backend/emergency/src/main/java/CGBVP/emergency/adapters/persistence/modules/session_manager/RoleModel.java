package CGBVP.emergency.adapters.persistence.modules.session_manager;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@RequiredArgsConstructor
@Entity
@Table(name = "ROL")
public class RoleModel {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "rol_id")
    private Long id;

    @Column(name = "nombre", nullable = false)
    private String name;

    @OneToMany(mappedBy = "role")
    private Set<PermissionModel> permission = new HashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        RoleModel roleModel = (RoleModel) o;
        return id != null && Objects.equals(id, roleModel.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
