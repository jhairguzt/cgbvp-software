package CGBVP.emergency.adapters.persistence.modules.logistic;

import CGBVP.emergency.intranet.logistic.application.port.in.UnitToRegister;
import CGBVP.emergency.intranet.logistic.domain.Unit;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UnitMapper {
    @Mapping(target = "id", source = "id")
    @Mapping(target = "state", source = "state")
    @Mapping(target = "firefighterCompany.id", source = "fireCompanyId")
    @Mapping(target = "plate", source = "plate")
    @Mapping(target = "imagePath", source = "imagePath")
    @Mapping(target = "createdAt", source = "createdAt")
    @Mapping(target = "updatedAt", source = "updatedAt")
    @Mapping(target = "code", source = "code")
    @Mapping(target = "description", source = "description")
    @Mapping(target = "type", source = "type")
    Unit toUser(UnitModel model);
    List<Unit> toUserList(List<UnitModel> unitModels);

    @Mapping(target = "fireCompanyId", source = "firefighterCompanyId")
    @Mapping(target = "description", source = "description")
    @Mapping(target = "type", source = "type")
    @Mapping(target = "code", source = "code")
    @Mapping(target = "plate", source = "plate")
    @Mapping(target = "imagePath", source = "imagePath")
    UnitModel toModel(UnitToRegister userToRegister);
}
