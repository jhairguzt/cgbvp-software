package CGBVP.emergency.adapters.persistence.modules.transcriber;

import CGBVP.emergency.adapters.persistence.modules.session_manager.UserMapper;
import CGBVP.emergency.intranet.transcriber.application.port.in.NewMessageToRegister;
import CGBVP.emergency.intranet.transcriber.domain.Message;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring",  uses = {UserMapper.class})
public interface MessageMapper {
    @Mapping(target = "code", source = "code")
    @Mapping(target = "type", source = "type")
    @Mapping(target = "emergency.id", source = "emergencyId")
    @Mapping(target = "user.id", source = "userId")
    MessageModel toModel(NewMessageToRegister newMessageToRegister);

    @Mapping(target = "id", source = "id")
    @Mapping(target = "type", source = "type")
    @Mapping(target = "content", source = "content")
    @Mapping(target = "emergency", source = "emergency")
    @Mapping(target = "user", source = "user")
    @Mapping(target = "deleted", source = "deleted")
    @Mapping(target = "code", source = "code")
    @Mapping(target = "createdAt", source = "createdAt")
    @Mapping(target = "updatedAt", source = "updatedAt")
    Message toMessage(MessageModel model);
    List<Message> toMessageList(List<MessageModel> messageModels);
}
