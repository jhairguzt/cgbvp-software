package CGBVP.emergency.adapters.persistence.modules.emergency;

import CGBVP.emergency.adapters.persistence.modules.session_manager.UserModel;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;

@Getter
@Setter
@RequiredArgsConstructor
@Entity
@Table(name = "USUARIO_X_EMERGENCIA")
public class UserXEmergencyModel {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_x_emergency_id")
    private Long id;

    @Column(name = "usuario_id", nullable = false)
    private Long userId;

    @Column(name = "emergencias_id", nullable = false)
    private Long emergencyId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usuario_id", insertable = false, updatable = false)
    private UserModel users;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "emergencias_id", insertable = false, updatable = false)
    private EmergencyModel emergency;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        UserXEmergencyModel that = (UserXEmergencyModel) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}

