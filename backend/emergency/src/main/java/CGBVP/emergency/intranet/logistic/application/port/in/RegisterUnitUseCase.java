package CGBVP.emergency.intranet.logistic.application.port.in;

import CGBVP.emergency.intranet.logistic.domain.Unit;

public interface RegisterUnitUseCase {
    Unit execute(UnitToRegister unitToRegister);
}
