package CGBVP.emergency.intranet.transcriber.application.port.out;

import CGBVP.emergency.intranet.transcriber.domain.DefaultText;

import java.util.List;

public interface GetDefaultTextListPort {
    List<DefaultText> getList(String text);
    List<DefaultText> getList();
}
