package CGBVP.emergency.intranet.transcriber.application;

import CGBVP.emergency.commons.hexagonal.UseCase;
import CGBVP.emergency.intranet.transcriber.application.port.in.GetDefaultTextListUseCase;
import CGBVP.emergency.intranet.transcriber.application.port.out.GetDefaultTextListPort;
import CGBVP.emergency.intranet.transcriber.domain.DefaultText;
import lombok.RequiredArgsConstructor;

import java.util.List;

@UseCase
@RequiredArgsConstructor
public class GetDefaultTextListService implements GetDefaultTextListUseCase {
    private final GetDefaultTextListPort getDefaultTextListPort;
    @Override
    public List<DefaultText> execute(String text) {
        return getDefaultTextListPort.getList(text);
    }

    @Override
    public List<DefaultText> execute() {
        return getDefaultTextListPort.getList();
    }
}
