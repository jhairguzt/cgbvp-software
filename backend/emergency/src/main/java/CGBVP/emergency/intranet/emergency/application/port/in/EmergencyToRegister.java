package CGBVP.emergency.intranet.emergency.application.port.in;

import CGBVP.emergency.intranet.emergency.domain.EmergencyState;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EmergencyToRegister {
    private Long companyId;
    private Date date;
    private String district;
    private String address;
    private String type;
    private String subType;
    private String description;
}
