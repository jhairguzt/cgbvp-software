package CGBVP.emergency.intranet.session_manager.domain;

import lombok.Data;

import java.sql.Timestamp;
import java.util.Date;

@Data
public class User {
    private long id;
    private String name;
    private String lastname;
    private String email;
    private String contactNumber;
    private String gender;
    private String dni;
    private String bloodType;
    private String position;
    private boolean active;
    private Date dateBirth;
    private Timestamp createdAt;
    private Timestamp updatedAt;
    private String role;
}
