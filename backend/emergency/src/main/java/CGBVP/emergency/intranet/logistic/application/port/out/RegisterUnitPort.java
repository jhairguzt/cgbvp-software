package CGBVP.emergency.intranet.logistic.application.port.out;

import CGBVP.emergency.intranet.logistic.application.port.in.UnitToRegister;
import CGBVP.emergency.intranet.logistic.domain.Unit;

public interface RegisterUnitPort {
    Unit register(UnitToRegister unitToRegister);
}
