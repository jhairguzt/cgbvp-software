package CGBVP.emergency.intranet.emergency.application.port;

import CGBVP.emergency.commons.hexagonal.UseCase;
import CGBVP.emergency.intranet.emergency.application.port.in.GetEmergencyListUseCase;
import CGBVP.emergency.intranet.emergency.application.port.out.GetEmergencyListPort;
import CGBVP.emergency.intranet.emergency.domain.Emergency;
import lombok.RequiredArgsConstructor;

import java.util.List;

@UseCase
@RequiredArgsConstructor
public class GetEmergencyListService implements GetEmergencyListUseCase {
    private final GetEmergencyListPort getEmergencyListPort;
    @Override
    public List<Emergency> execute(String state) {
        return getEmergencyListPort.getList(state);
    }
}
