package CGBVP.emergency.intranet.transcriber.application;

import CGBVP.emergency.commons.hexagonal.UseCase;
import CGBVP.emergency.external.awsS3.AwsStorageExternalService;
import CGBVP.emergency.intranet.transcriber.application.port.in.ContentToRegister;
import CGBVP.emergency.intranet.transcriber.application.port.in.NewMessageToRegister;
import CGBVP.emergency.intranet.transcriber.application.port.in.RegisterNewMessageUseCase;
import CGBVP.emergency.intranet.transcriber.application.port.out.RegisterContentPort;
import CGBVP.emergency.intranet.transcriber.application.port.out.RegisterNewMessagePort;
import CGBVP.emergency.intranet.transcriber.domain.Message;
import lombok.RequiredArgsConstructor;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.transaction.Transactional;
import java.util.UUID;

import java.io.File;

@UseCase
@Transactional
@RequiredArgsConstructor
public class RegisterNewMessageService implements RegisterNewMessageUseCase {
    private final RegisterNewMessagePort registerNewMessagePort;
    private final RegisterContentPort registerContentPort;
    private final AwsStorageExternalService awsStorageExternalService;

    @Override
    public Message execute(NewMessageToRegister toRegister) {
        try{
            String randomUUID = UUID.randomUUID().toString().replaceAll("_","");
            File file = new File(randomUUID);

            //voiceNote.transferTo(file);
            String basePathTranscriptions = "/transcriptions";
            String filePath = basePathTranscriptions + "/" + randomUUID;
            //var fileKey = awsStorageExternalService.saveFile(file, basePathTranscriptions);
            //var filePath = awsStorageExternalService.getUrlFile(fileKey);
            ContentToRegister contentToRegister = new ContentToRegister(toRegister.getDefaultTextId(), toRegister.getIntranetTranscription(), toRegister.getExtranetTranscription(), filePath);
            var newContent = registerContentPort.register(contentToRegister);
            return registerNewMessagePort.register(toRegister, newContent.getId());
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw e;
        }
    }

}
