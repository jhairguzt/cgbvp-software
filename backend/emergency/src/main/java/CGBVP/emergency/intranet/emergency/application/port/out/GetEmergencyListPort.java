package CGBVP.emergency.intranet.emergency.application.port.out;

import CGBVP.emergency.intranet.emergency.domain.Emergency;

import java.util.List;

public interface GetEmergencyListPort {
    List<Emergency> getList(String state);
}
