package CGBVP.emergency.intranet.session_manager.application.port.in;

import CGBVP.emergency.intranet.session_manager.domain.Role;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserToRegister {
    private String name;
    private String lastname;
    private String email;
    private String contactNumber;
    private String gender;
    private String dni;
    private String bloodType;
    private String position;
    private Date dateBirth;
    private Long roleId;
}
