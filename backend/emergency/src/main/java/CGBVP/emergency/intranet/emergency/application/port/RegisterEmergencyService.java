package CGBVP.emergency.intranet.emergency.application.port;

import CGBVP.emergency.commons.hexagonal.UseCase;
import CGBVP.emergency.intranet.emergency.application.port.in.RegisterEmergencyUseCase;
import CGBVP.emergency.intranet.emergency.application.port.in.EmergencyToRegister;
import CGBVP.emergency.intranet.emergency.application.port.out.RegisterEmergencyPort;
import CGBVP.emergency.intranet.emergency.domain.Emergency;
import lombok.RequiredArgsConstructor;

@UseCase
@RequiredArgsConstructor
public class RegisterEmergencyService implements RegisterEmergencyUseCase {
    private final RegisterEmergencyPort registerEmergencyPort;
    @Override
    public Emergency execute(EmergencyToRegister emergencyToRegister) {
        return registerEmergencyPort.register(emergencyToRegister);
    }
}
