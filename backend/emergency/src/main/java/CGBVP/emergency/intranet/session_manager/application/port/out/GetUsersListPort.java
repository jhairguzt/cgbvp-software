package CGBVP.emergency.intranet.session_manager.application.port.out;

import CGBVP.emergency.intranet.session_manager.domain.User;

import java.util.List;

public interface GetUsersListPort {
    List<User> getList(String name);
    List<User> getList();
}
