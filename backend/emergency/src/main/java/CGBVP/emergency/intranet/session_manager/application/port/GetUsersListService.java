package CGBVP.emergency.intranet.session_manager.application.port;

import CGBVP.emergency.commons.hexagonal.UseCase;
import CGBVP.emergency.intranet.session_manager.application.port.in.GetUsersListUseCase;
import CGBVP.emergency.intranet.session_manager.application.port.out.GetUsersListPort;
import CGBVP.emergency.intranet.session_manager.domain.User;
import lombok.RequiredArgsConstructor;

import java.util.List;

@UseCase
@RequiredArgsConstructor
public class GetUsersListService implements GetUsersListUseCase {
    private final GetUsersListPort getUsersListPort;

    @Override
    public List<User> execute(String name) {
        return getUsersListPort.getList(name);
    }

    @Override
    public List<User> execute() {
        return getUsersListPort.getList();
    }
}
