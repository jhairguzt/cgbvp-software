package CGBVP.emergency.intranet.transcriber.application.port.in;

import CGBVP.emergency.intranet.transcriber.domain.Message;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface RegisterNewMessageUseCase {
    Message execute(NewMessageToRegister toRegister);
}
