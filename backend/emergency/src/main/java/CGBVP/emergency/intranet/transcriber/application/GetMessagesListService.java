package CGBVP.emergency.intranet.transcriber.application;

import CGBVP.emergency.commons.hexagonal.UseCase;
import CGBVP.emergency.intranet.transcriber.application.port.in.GetMessagesListUseCase;
import CGBVP.emergency.intranet.transcriber.application.port.out.GetMessagesListPort;
import CGBVP.emergency.intranet.transcriber.domain.Message;
import lombok.RequiredArgsConstructor;

import java.util.List;

@UseCase
@RequiredArgsConstructor
public class GetMessagesListService implements GetMessagesListUseCase {
    private final GetMessagesListPort getMessagesListPort;
    @Override
    public List<Message> execute(Long emergencyId) {
        return getMessagesListPort.getList(emergencyId);
    }
}
