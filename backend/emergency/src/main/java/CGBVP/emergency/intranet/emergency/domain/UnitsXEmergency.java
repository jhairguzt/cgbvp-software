package CGBVP.emergency.intranet.emergency.domain;

import CGBVP.emergency.intranet.logistic.domain.Unit;
import lombok.Data;

@Data
public class UnitsXEmergency {
    private long id;
    private Unit unit;
    private Emergency emergency;
}
