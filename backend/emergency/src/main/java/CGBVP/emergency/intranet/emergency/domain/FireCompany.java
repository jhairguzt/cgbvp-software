package CGBVP.emergency.intranet.emergency.domain;

import CGBVP.emergency.intranet.logistic.domain.Unit;
import lombok.Data;

import java.util.List;
@Data
public class FireCompany {
    private long id;
    private String manager;
    private String name;
    private String district;
    private String address;
    private List<Unit> unitsList;
    private boolean state;
}
