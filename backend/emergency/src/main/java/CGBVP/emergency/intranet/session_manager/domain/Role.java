package CGBVP.emergency.intranet.session_manager.domain;

import lombok.Data;

import java.util.List;

@Data
public class Role {
    private long id;
    private String name;
    private List<Permission> permissions;
}
