package CGBVP.emergency.intranet.transcriber.application.port.out;

import CGBVP.emergency.intranet.transcriber.application.port.in.ContentToRegister;
import CGBVP.emergency.intranet.transcriber.domain.Content;

public interface RegisterContentPort {
    Content register(ContentToRegister contentToRegister);
}
