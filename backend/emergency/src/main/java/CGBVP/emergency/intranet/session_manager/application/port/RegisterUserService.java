package CGBVP.emergency.intranet.session_manager.application.port;

import CGBVP.emergency.commons.hexagonal.UseCase;
import CGBVP.emergency.intranet.session_manager.application.port.in.RegisterUserUseCase;
import CGBVP.emergency.intranet.session_manager.application.port.in.UserToRegister;
import CGBVP.emergency.intranet.session_manager.application.port.out.RegisterUserPort;
import CGBVP.emergency.intranet.session_manager.domain.User;
import lombok.RequiredArgsConstructor;

@UseCase
@RequiredArgsConstructor
public class RegisterUserService implements RegisterUserUseCase {
    private final RegisterUserPort registerUserPort;

    @Override
    public User execute(UserToRegister userToRegister) {
        return registerUserPort.register(userToRegister);
    }
}
