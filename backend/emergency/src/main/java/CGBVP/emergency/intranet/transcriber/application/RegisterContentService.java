package CGBVP.emergency.intranet.transcriber.application;

import CGBVP.emergency.commons.hexagonal.UseCase;
import CGBVP.emergency.intranet.transcriber.application.port.in.ContentToRegister;
import CGBVP.emergency.intranet.transcriber.application.port.in.RegisterContentUseCase;
import CGBVP.emergency.intranet.transcriber.application.port.out.RegisterContentPort;
import CGBVP.emergency.intranet.transcriber.domain.Content;
import lombok.RequiredArgsConstructor;

@UseCase
@RequiredArgsConstructor
public class RegisterContentService implements RegisterContentUseCase {
    private final RegisterContentPort registerContentPort;
    @Override
    public Content execute(ContentToRegister contentToRegister) {
        return registerContentPort.register(contentToRegister);
    }
}
