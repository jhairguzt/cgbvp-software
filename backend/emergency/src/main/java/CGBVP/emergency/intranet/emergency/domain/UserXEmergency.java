package CGBVP.emergency.intranet.emergency.domain;

import CGBVP.emergency.intranet.session_manager.domain.User;
import lombok.Data;

@Data
public class UserXEmergency {
    private long id;
    private Emergency emergency;
    private User users;
}
