package CGBVP.emergency.intranet.transcriber.application.port.in;

import CGBVP.emergency.intranet.transcriber.domain.Content;

public interface RegisterContentUseCase {
    Content execute(ContentToRegister contentToRegister);
}
