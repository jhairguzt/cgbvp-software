import Head from "next/head";
import React from "react";
import HeaderCGBVP from "../Header/HeaderCGBVP";

// eslint-disable-next-line react/prop-types
export default function PageLayout({ children , title = 'CGBVP'}){
  return(
      <>
        <Head>
          <meta
              name="viewport"
              content="width=device-width, initial-scale=1, shrink-to-fit=no"
          />
          <title>{title}</title>
          <link rel="icon" href={"/favicon.ico"}/>
        </Head>
        <HeaderCGBVP
            color="primary"
            changeColorOnScroll={{
              height: 400,
              color: "white",
            }}
        >
          {children}
        </HeaderCGBVP>
      </>
  );
}
