import {fetcher, useConfig} from './config';
import useSWR from 'swr';
import axios from "axios";

export function getDefaultTextList() {
  const {apiUrl} = useConfig();
  const {data, error} = useSWR([apiUrl, `messages/defaultTextList`], fetcher);

  return {
    defaultTextList: data,
    isLoading: !error && !data,
    error,
  };
}

export function getDefaultTextListByText(text) {
  const {apiUrl} = useConfig();
  const {data, error} = useSWR([apiUrl, `messages/defaultTextList/${text}`], fetcher);

  return {
    defaultTextList: data,
    isLoading: !error && !data,
    error,
  };
}

export function getMessagesList(emergencyId) {
  const {apiUrl} = useConfig();
  const {data, error} = useSWR([apiUrl, `messages/list/${emergencyId}`], fetcher);

  return {
    messagesList: data,
    isLoading: !error && !data,
    error,
  };
}

export function newMessagesList(type, code, defaultText, message, userId, emergencyId) {
  const {apiUrl} = useConfig();
  let body = {
    "code": code,
    "defaultTextId": defaultText,
    "emergencyId": emergencyId,
    "extranetTranscription": message,
    "intranetTranscription": message,
    "type": type,
    "userId": userId,
  };
  console.log(body);
  const res = axios.post(`${apiUrl}/messages/new`, body);

  return res;
}
