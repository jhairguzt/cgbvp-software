import getConfig from 'next/config';
import axios from 'axios';

export function useConfig() {
  const {publicRuntimeConfig: {apiUrl}} = getConfig();

  return {
    apiUrl,
  };
}

export async function fetcher(baseUrl, uri, params={}, options={}) {
  const {data} = await axios.get(`${baseUrl}/${uri}`, {
    params,
    ...options,
  });

  return data;
}
