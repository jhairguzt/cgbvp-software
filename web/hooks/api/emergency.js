import {fetcher, useConfig} from './config';
import useSWR from 'swr';

export function getEmergencyList(state) {
  const {apiUrl} = useConfig();

  const {data, error} = useSWR([apiUrl, `emergency/list/${state}`], fetcher);

  return {
    emergencyList: data,
    isLoading: !error && !data,
    error,
  };
}
