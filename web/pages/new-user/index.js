import React from "react";
// nodejs library that concatenates classes
//import classNames from "classnames";
// react components for routing our app without refresh
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import styles from "../../styles/jss/nextjs-material-kit/homeStyle";
import PageLayout from "../../components/PageLayout/PageLayout";
import {Container} from "@material-ui/core";
import {useRouter} from "next/router";
import Button from "../../components/CustomButtons/Button";
import Card from "../../components/Card/Card";
import UserFormCardBody from "../../components/Card/UserFormCardBody";

const useStyles = makeStyles(styles);


export default function editUser() {
  const classes = useStyles();
  const router = useRouter();
  const {id: userId} = router.query;

  if (!userId) {
    return (
        <div>ERROR</div>
    );
  }

  return (
      <div>
        <PageLayout title={"Editar usuario"}>
          <Container>
            <div className={classes.topComponents}>
              <div></div>
              <Button color="primary" style={{maxHeight: "30px", marginLeft:"10px"}}>Volver</Button>
            </div>
            <div className={classes.cardFlexContainer}>
              <Card style={{maxWidth: "700px"}}>
                <UserFormCardBody></UserFormCardBody>
              </Card>
            </div>
          </Container>
        </PageLayout>
      </div>
  );
}