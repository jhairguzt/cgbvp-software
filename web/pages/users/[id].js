import React from "react";
// nodejs library that concatenates classes
//import classNames from "classnames";
// react components for routing our app without refresh
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import styles from "../../styles/jss/nextjs-material-kit/homeStyle";
import PageLayout from "../../components/PageLayout/PageLayout";
import {Container} from "@material-ui/core";
import Link from "next/link";
import {useRouter} from "next/router";
import Button from "../../components/CustomButtons/Button";
import Card from "../../components/Card/Card";
import UserCardBody from "../../components/Card/UserCardBody";
import {getUsersById} from "../../hooks/api/user";

const useStyles = makeStyles(styles);


export default function userPage() {
  const classes = useStyles();
  const router = useRouter();
  const {id: userId} = router.query;

  if (!userId) {
    return (
        <div>ERROR</div>
    );
  }
  const {user, isLoading, error} = getUsersById(userId);

  return (
      <div>
        <PageLayout title={"Usuario"}>
          <Container>
            <div className={classes.topComponents}>
              <div></div>
              <Link href="/users">
                <Button color="primary" style={{maxHeight: "30px", marginLeft:"10px"}}>Ver todos los usuarios</Button>
              </Link>
            </div>
            <div className={classes.cardFlexContainer}>
              <Card style={{maxWidth: "500px"}}>
                {!isLoading && <UserCardBody data={user}></UserCardBody>}
              </Card>
            </div>
          </Container>
        </PageLayout>
      </div>
  );
}