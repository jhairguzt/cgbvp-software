import React from "react";

import { makeStyles } from "@material-ui/core/styles";
import styles from "../../../styles/jss/nextjs-material-kit/homeStyle";
import PageLayout from "../../../components/PageLayout/PageLayout";
import Button from "../../../components/CustomButtons/Button";
import Link from "next/link";
import Primary from "../../../components/Typography/Primary";
import {Container} from "@material-ui/core";
import TimeLine from "../../../components/TimeLine/TimeLine";
import {getMessagesList} from "../../../hooks/api/messages";
import {useRouter} from "next/router";


const useStyles = makeStyles(styles);


export default function emergencyPage() {
  const classes = useStyles();
  const router = useRouter();
  const {id: emergencyId} = router.query;

  if (!emergencyId) {
    return (
        <div>ERROR</div>
    );
  }

  const {messagesList, error, isLoading} = getMessagesList(emergencyId);

  return (
      <div>
        <PageLayout title={"Emergencias"}>
          <Container>
            <div className={classes.topComponents}>
              <Primary>
                <h4  style={{lineHeight: "1em", fontWeight:"600"}}>LISTA DE MENSAJES</h4>
              </Primary>
              <Link href={"/emergency/" + emergencyId + "/create-message"}>
                <Button style={{maxHeight: "30px", maxWidth: "150px", marginLeft:"10px"}}>+ Agregar mensaje</Button>
              </Link>
            </div>
            <div style={{justifyContent: "initial"}}>
              <TimeLine data={messagesList} loading={isLoading}></TimeLine>
            </div>
          </Container>
        </PageLayout>
      </div>
  );
}