const homeStyle = {
  topComponents: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    lineHeight: "0px"
  },
  topTabsContainer:{
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    marginTop: "20px"
  },
  cardsContainer:{
    marginTop: "20px",
    display: "inherit",
  },
  cardFlexContainer:{
    marginTop: "20px",
    display: "flex",
    justifyContent: "center",
  },
  tableContainer:{
    marginTop: "20px",
  },
  tableHeader: {
    backgroundColor: "#C00812",
  }
};

export default homeStyle;